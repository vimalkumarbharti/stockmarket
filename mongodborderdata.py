import gzip
from datetime import datetime
from pymongo import MongoClient
client = MongoClient(port=27017)
db=client.iimuproject
rawFile = gzip.open("rawdata/CASH_Orders_01042013.DAT.gz", "rb")
print("Start Time =", datetime.now().strftime("%H:%M:%S"))
i=0
for eachLine in rawFile:
    i=i+1
    recordIndicator = eachLine[0:2].decode('utf8')
    segment = eachLine[2:6].decode('utf8')
    orderNumber = eachLine[6:22].decode('utf8')
    transactionTime = eachLine[22:36].decode('utf8')
    buySellIndicator = eachLine[36:37].decode('utf8')
    activityType = eachLine[37:38].decode('utf8')
    symbol = eachLine[38:48].decode('utf8')
    series = eachLine[48:50].decode('utf8')
    volumeDisclosed = eachLine[50:58].decode('utf8')
    volumeOriginal = eachLine[58:66].decode('utf8')
    limitPrice = eachLine[66:74].decode('utf8')
    triggerPrice = eachLine[74:82].decode('utf8')
    marketOrderFlag = eachLine[82:83].decode('utf8')
    stopLossFlag = eachLine[83:84].decode('utf8')
    iOFlag  = eachLine[84:85].decode('utf8')
    algoIndicator =  eachLine[85:86].decode('utf8')
    clientIdentity =  eachLine[86:87].decode('utf8')
    eachorder = {
        'recordIndicator':recordIndicator ,
        'segment': segment,
        'orderNumber':orderNumber,
        'transactionTime': transactionTime,
        'buySellIndicator': buySellIndicator,
        'activityType':activityType ,
        'symbol': symbol,
        'series': series,
        'volumeDisclosed':volumeDisclosed ,
        'volumeOriginal': volumeOriginal,
        'limitPrice':limitPrice ,
        'triggerPrice':triggerPrice ,
        'marketOrderFlag': marketOrderFlag,
        'stopLossFlag': stopLossFlag,
        'iOFlag':iOFlag ,
        'algoIndicator':algoIndicator ,
        'clientIdentity':clientIdentity
    }
    result=db.orderraw.insert_one(eachorder)
   # if i > 100:
   #   break;
    
rawFile.close()
print("End Time =", datetime.now().strftime("%H:%M:%S"))
    


