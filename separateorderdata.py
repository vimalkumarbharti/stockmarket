# -*- coding: utf-8 -*-
import gzip
from datetime import datetime
rawFile = gzip.open("rawdata/CASH_Orders_01042013.DAT.gz", "rb")
companyfiles = {}
i=0
print("Start Time =", datetime.now().strftime("%H:%M:%S"))
for eachLine in rawFile:
    i=i+1
    """
    recordIndicator = eachLine[0:2]
    segment = eachLine[2:6]
    orderNumber = eachLine[6:22]
    transactionTime = eachLine[22:36]
    buySellIndicator = eachLine[36:37]
    activityType = eachLine[37:38]
    """
    symbol = eachLine[38:48]
    companyName = symbol.decode('utf8').lstrip("b")
    if companyName in companyfiles.keys():
        companyFileName = companyfiles[companyName][0]
        companyFileObj = companyfiles[companyName][1]
    else:
        companyFileName = "companyorderdata/raw/"+companyName+"_CASH_Orders.DAT.gz"
        companyFileObj = gzip.open(companyFileName, "wb")
        companyfiles[companyName] = [companyFileName,companyFileObj]
  
    companyFileObj.write(eachLine)
  # print(companyName)
    #if i > 100:
    #  break;
for companydata in companyfiles:  
    companyfiles[companydata][1].close()
rawFile.close()
print("End Time =", datetime.now().strftime("%H:%M:%S"))
    
