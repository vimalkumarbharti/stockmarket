from datetime import datetime
import mysql.connector

mydb = mysql.connector.connect(
  host="localhost",
  port="3308",
  user="root",
  password="asd",
  database="iimu"
)
mycursor = mydb.cursor(buffered=True)
print("Start Time =", datetime.now().strftime("%H:%M:%S"))
i=0
mycursor.execute('SELECT * FROM orderupdated order by updatedTradeId ASC');
allOrderUpdated = mycursor.fetchall()
mycursor.reset()
previousId = 0 
for eachOrderUpdated in allOrderUpdated :
   
    curentId = eachOrderUpdated[18]
    if curentId == previousId:
        continue
    i=i+1
    mycursor.execute("SELECT * FROM orderraw  where id > %s AND id <= %s order by id ASC",(previousId,curentId,));
    orderraw = mycursor.fetchall()
    for eachOrder1 in orderraw:
        sql = "insert into ordermarged (recordIndicator,segment,orderNumber,transactionTime,buySellIndicator, activityType,symbol,series,volumeDisclosed,volumeOriginal,limitPrice,triggerPrice,marketOrderFlag,stopLossFlag,iOFlag,algoIndicator,clientIdentity) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
        val = (eachOrder1[1],eachOrder1[2],eachOrder1[3],eachOrder1[4], eachOrder1[5],eachOrder1[6],eachOrder1[7],eachOrder1[8],eachOrder1[9],eachOrder1[10],eachOrder1[11],eachOrder1[12],eachOrder1[13],eachOrder1[14],eachOrder1[15],eachOrder1[16],eachOrder1[17])
        mycursor.execute(sql, val)
    mycursor.execute("SELECT * FROM orderupdated  where updatedTradeId = %s order by updatedTradeId ASC",(curentId,));
    orderupdated = mycursor.fetchall()
    for eachOrder1 in orderupdated:
        sql = "insert into ordermarged (recordIndicator,segment,orderNumber,transactionTime,buySellIndicator, activityType,symbol,series,volumeDisclosed,volumeOriginal,limitPrice,triggerPrice,marketOrderFlag,stopLossFlag,iOFlag,algoIndicator,clientIdentity,buyOrSell) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
        val = (eachOrder1[1],eachOrder1[2],eachOrder1[3],eachOrder1[4], eachOrder1[5],eachOrder1[6],eachOrder1[7],eachOrder1[8],eachOrder1[9],eachOrder1[10],eachOrder1[11],eachOrder1[12],eachOrder1[13],eachOrder1[14],eachOrder1[15],eachOrder1[16],eachOrder1[17],eachOrder1[19])
        mycursor.execute(sql, val)
    previousId = curentId
    if i > 500:
        break;
mydb.commit()
mycursor.close()
mydb.close() 
print("End Time =", datetime.now().strftime("%H:%M:%S"))