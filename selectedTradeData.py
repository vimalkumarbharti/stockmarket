import gzip
from datetime import datetime
import mysql.connector

mydb = mysql.connector.connect(
  host="localhost",
  port="3308",
  user="root",
  password="asd",
  database="iimu"
)
mycursor = mydb.cursor()
rawFile = gzip.open("companytradedata/raw/SBIN_CASH_Trades.DAT.gz", "rb")
print("Start Time =", datetime.now().strftime("%H:%M:%S"))
i=0
requiredOrder = [67314834,67348420,67359643,67345084,67345738,67312004,67317896,67279155,67307232,67277950,67513456,67511612,67261033,67384699,67707347,67478027,67261020,67472677,67124887,67123149,66758174,66758172,67194401,66878998,66768357,66768472,66892841,66927235,66927300,66927303,66946938,66966102,66948880,67012710,66995528]
sql = "insert into traderequireddata (recordIndicator,segment,tradeNumber,tradeTime,symbol,series,tradePrice,tradeQuantity,buyOrderNumber,buyAlgoIndicator,buyClientIdentityFlag,sellOrderNumber,sellAlgoIndicator,sellClientIdentityFlag) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
for eachLine in rawFile:
    i=i+1
    tempOrderNumber  = int(eachLine[72:80].decode('utf8'))
 #   print(tempOrderNumber)
    if tempOrderNumber in requiredOrder: 
        recordIndicator = eachLine[0:2].decode('utf8')
        segment = eachLine[2:6].decode('utf8')
        tradeNumber = eachLine[6:22].decode('utf8')
        tradeTime = eachLine[22:36].decode('utf8')
        symbol = eachLine[36:46].decode('utf8')
        series = eachLine[46:48].decode('utf8')
        tradePrice  = eachLine[48:56].decode('utf8')
        tradeQuantity  = eachLine[56:64].decode('utf8')
        buyOrderNumber  = eachLine[64:80].decode('utf8')
        buyAlgoIndicator = eachLine[80:81].decode('utf8')
        buyClientIdentityFlag = eachLine[81:82].decode('utf8')
        sellOrderNumber = eachLine[82:98].decode('utf8')
        sellAlgoIndicator = eachLine[98:99].decode('utf8')
        sellClientIdentityFlag = eachLine[99:100].decode('utf8')
        val = (recordIndicator,segment,tradeNumber,tradeTime,symbol,series,tradePrice,tradeQuantity,buyOrderNumber,buyAlgoIndicator,buyClientIdentityFlag,sellOrderNumber,sellAlgoIndicator,sellClientIdentityFlag)
        mycursor.execute(sql, val)
#    if i > 100:
#      break;
mydb.commit()
mycursor.close()
mydb.close()   
rawFile.close()
print("End Time =", datetime.now().strftime("%H:%M:%S"))
    


