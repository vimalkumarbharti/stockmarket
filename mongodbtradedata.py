import gzip
from datetime import datetime
from pymongo import MongoClient
client = MongoClient(port=27017)
db=client.iimuproject
rawFile = gzip.open("rawdata/CASH_Trades_01042013.DAT.gz", "rb")
print("Start Time =", datetime.now().strftime("%H:%M:%S"))
i=0
for eachLine in rawFile:
    i=i+1
    recordIndicator = eachLine[0:2].decode('utf8')
    segment = eachLine[2:6].decode('utf8')
    tradeNumber = eachLine[6:22].decode('utf8')
    tradeTime = eachLine[22:36].decode('utf8')
    symbol = eachLine[36:46].decode('utf8')
    series = eachLine[46:48].decode('utf8')
    tradePrice  = eachLine[48:56].decode('utf8')
    tradeQuantity  = eachLine[56:64].decode('utf8')
    buyOrderNumber  = eachLine[64:80].decode('utf8')
    buyAlgoIndicator = eachLine[80:81].decode('utf8')
    buyClientIdentityFlag = eachLine[81:82].decode('utf8')
    sellOrderNumber = eachLine[82:98].decode('utf8')
    sellAlgoIndicator = eachLine[98:99].decode('utf8')
    sellClientIdentityFlag = eachLine[99:100].decode('utf8')
    
    eachTrade = {
        'recordIndicator':recordIndicator ,
        'segment': segment,
        'tradeNumber':tradeNumber ,
        'tradeTime': tradeTime,
        'symbol': symbol,
        'series': series,
        'tradePrice':tradePrice ,
        'tradeQuantity': tradeQuantity,
        'buyOrderNumber':buyOrderNumber ,
        'buyAlgoIndicator':buyAlgoIndicator ,
        'buyClientIdentityFlag': buyClientIdentityFlag,
        'sellOrderNumber': sellOrderNumber,
        'sellAlgoIndicator':sellAlgoIndicator ,
        'sellClientIdentityFlag':sellClientIdentityFlag
    }
    result=db.traderaw.insert_one(eachTrade)
    #if i > 100:
    #  break;
    
rawFile.close()
print("End Time =", datetime.now().strftime("%H:%M:%S"))
    


