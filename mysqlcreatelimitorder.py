from datetime import date, datetime, time, timedelta
import mysql.connector

mydb = mysql.connector.connect(
  host="localhost",
  port="3308",
  user="root",
  password="asd",
  database="iimu"
)
mycursor = mydb.cursor(buffered=True)
print("Start Time =", datetime.now().strftime("%H:%M:%S"))
startJiffiesSecond = datetime(1980, 1, 1, 0, 0, 0)
'''
Tranjuction Start: 2013-04-01 13:26:52.180331
Tranjuction End:   2013-04-01 20:23:46.305928
'''
transuctionDate = datetime(2013, 4, 1, 15, 15, 16)
dateDiff = transuctionDate - startJiffiesSecond
dateSec = dateDiff.total_seconds();
jiffiesSecond = dateSec*65535;
#jiffiesSecond = 68765279892319
i=0
print(jiffiesSecond)
sql = "SELECT * FROM ordermarged where transactionTime <= %s  order by   orderNumber ASC, transactionTime DESC,activityType DESC"

mycursor.execute(sql, (jiffiesSecond,) )
allOrderM = mycursor.fetchall()
curOrdNum = 0;
previous = 0;
for eachOrder1 in allOrderM:
    previous = curOrdNum
    curOrdNum = eachOrder1[3]
    if curOrdNum == previous:
        continue
    tempMarketOrderFlag = eachOrder1[13]
    tempActivityType = int(eachOrder1[6])
    tempiOFlag = eachOrder1[15]
    tempStopLossFlag = eachOrder1[14]
    tempSeries = eachOrder1[8]
    if tempMarketOrderFlag == 'N' and ( tempActivityType == 1 or tempActivityType == 4 or tempActivityType == 6) and tempiOFlag == 'N' and tempStopLossFlag == 'N' and tempSeries == 'EQ':
        jiffiesSecond = eachOrder1[4]/65535.0
        formatedTime = startJiffiesSecond + timedelta(seconds=jiffiesSecond)
        formatedTime = formatedTime.time()
        orderNo = str(eachOrder1[3])[8:16]
        sql = "insert into limitorder (recordIndicator,segment,orderNumber,transactionTime,formatedTime,buySellIndicator, activityType,symbol,series,volumeDisclosed,volumeOriginal,limitPrice,triggerPrice,marketOrderFlag,stopLossFlag,iOFlag,algoIndicator,clientIdentity) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
        val = (eachOrder1[1],eachOrder1[2],orderNo,eachOrder1[4], formatedTime,eachOrder1[5],eachOrder1[6],eachOrder1[7],eachOrder1[8],eachOrder1[9],eachOrder1[10],eachOrder1[11],eachOrder1[12],eachOrder1[13],eachOrder1[14],eachOrder1[15],eachOrder1[16],eachOrder1[17])
        mycursor.execute(sql, val)
        i = i + 1
   # if i > 100:
   #     break;
mydb.commit()
mycursor.close()
mydb.close() 
print("End Time =", datetime.now().strftime("%H:%M:%S"))
    