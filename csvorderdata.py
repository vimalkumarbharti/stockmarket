import sys
import os.path
from os import path
import gzip
import csv
if len(sys.argv) > 1: 
    companyName = sys.argv[1]
    if companyName != "":
        companyName = companyName.upper()
        rawFile = "companyorderdata/raw/"+companyName+"_CASH_Orders.DAT.gz"
        if path.isfile(rawFile):
            rawFileObj = gzip.open(rawFile, "rb")
            i=0
            csvfile = "companyorderdata/csv/"+companyName+"_CASH_Orders.csv"
            with open(csvfile, 'w') as csvfileObj:
                writer = csv.writer(csvfileObj)
                row = ["Record Indicator","Segment","Order Number","Transaction Time","Buy Sell Indicator","Activity Type","Symbol","Series","Volume Disclosed","Volume Original","Limit Price","Trigger Price","Market Order Flag","Stop Loss Flag","IO Flag","Algo Indicator","Client Identity"]
                writer.writerow(row)
                for eachLine in rawFileObj:
                    recordIndicator = eachLine[0:2].decode('utf8')
                    segment = eachLine[2:6].decode('utf8')
                    orderNumber = eachLine[6:22].decode('utf8')
                    transactionTime = eachLine[22:36].decode('utf8')
                    buySellIndicator = eachLine[36:37].decode('utf8')
                    activityType = eachLine[37:38].decode('utf8')
                    symbol = eachLine[38:48].decode('utf8')
                    series = eachLine[48:50].decode('utf8')
                    volumeDisclosed = eachLine[50:58].decode('utf8')
                    volumeOriginal = eachLine[58:66].decode('utf8')
                    limitPrice = eachLine[66:74].decode('utf8')
                    triggerPrice = eachLine[74:82].decode('utf8')
                    marketOrderFlag = eachLine[82:83].decode('utf8')
                    stopLossFlag = eachLine[83:84].decode('utf8')
                    iOFlag  = eachLine[84:85].decode('utf8')
                    algoIndicator =  eachLine[85:86].decode('utf8')
                    clientIdentity =  eachLine[86:87].decode('utf8')
                    row = [recordIndicator,segment,orderNumber,transactionTime,buySellIndicator,activityType,symbol,series,volumeDisclosed,volumeOriginal,limitPrice,triggerPrice,marketOrderFlag,stopLossFlag,iOFlag,algoIndicator,clientIdentity]
                    writer.writerow(row)
                    i=i+1
            csvfileObj.close()
        else:
            print("Company Name Is Not Correct")
    else:
        print("Not Found")
else:
    print("Enter Company Name")



