import sqlite3
from datetime import datetime
conn = sqlite3.connect('iimsqlite.db')
print("Start Time =", datetime.now().strftime("%H:%M:%S"))
i=0

for eachTrade in conn.execute('SELECT * FROM traderaw'):
    i=i+1
    tempBuyOrderNumber = eachTrade[9]
    buyfound = 0
    sellfound = 0
    for eachOrder in conn.execute('SELECT * FROM orderraw WHERE orderNumber = ? order by transactionTime DESC LIMIT 0,1 ',(tempBuyOrderNumber,)):
        conn.execute("insert into orderupdated (recordIndicator,segment,orderNumber,transactionTime,buySellIndicator, activityType,symbol,series,volumeDisclosed,volumeOriginal,limitPrice,triggerPrice,marketOrderFlag,stopLossFlag,iOFlag,algoIndicator,clientIdentity,updatedTradeId) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,0)",(eachOrder[1],eachOrder[2],eachOrder[3],eachOrder[4], eachOrder[5],eachOrder[6],eachOrder[7],eachOrder[8],eachOrder[9],eachOrder[10],eachOrder[11],eachOrder[12],eachOrder[13],eachOrder[14],eachOrder[15],eachOrder[16],eachOrder[17]))
        tempTransactiontime = eachTrade[4]
        tempActivitytype = 5
        tempVolumeOriginal =  int(eachTrade[8])
        tempLimitPrice = eachTrade[7]
        conn.execute("insert into orderupdated (recordIndicator,segment,orderNumber,transactionTime,buySellIndicator, activityType,symbol,series,volumeDisclosed,volumeOriginal,limitPrice,triggerPrice,marketOrderFlag,stopLossFlag,iOFlag,algoIndicator,clientIdentity,updatedTradeId,buyOrSell) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,'buy')",(eachOrder[1],eachOrder[2],eachOrder[3],tempTransactiontime, eachOrder[5],tempActivitytype,eachOrder[7],eachOrder[8],eachOrder[9],tempVolumeOriginal,tempLimitPrice,eachOrder[12],eachOrder[13],eachOrder[14],eachOrder[15],eachOrder[16],eachOrder[17],eachTrade[0]))
        buyfound = 1
    # Sell Order Processing
    tempSellOrderNumber = eachTrade[12]
    for eachOrder in conn.execute('SELECT * FROM orderraw WHERE orderNumber = ? order by transactionTime DESC LIMIT 0,1 ',(tempSellOrderNumber,)):
        tempTransactiontime = eachTrade[4]
        tempActivitytype = 5
        tempVolumeOriginal =  int(eachOrder[10]) - int(eachTrade[8])
        tempLimitPrice = eachTrade[7]
        conn.execute("insert into orderupdated (recordIndicator,segment,orderNumber,transactionTime,buySellIndicator, activityType,symbol,series,volumeDisclosed,volumeOriginal,limitPrice,triggerPrice,marketOrderFlag,stopLossFlag,iOFlag,algoIndicator,clientIdentity,updatedTradeId,buyOrSell) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,'sell')",(eachOrder[1],eachOrder[2],eachOrder[3],tempTransactiontime, eachOrder[5],tempActivitytype,eachOrder[7],eachOrder[8],eachOrder[9],tempVolumeOriginal,tempLimitPrice,eachOrder[12],eachOrder[13],eachOrder[14],eachOrder[15],eachOrder[16],eachOrder[17],eachTrade[0]))
        sellfound = 1
    if buyfound == 0 :
        conn.execute("insert into buynotfound (orderNumber) values(?)",(tempBuyOrderNumber,))
    if sellfound == 0 :
        conn.execute("insert into sellnotfound (orderNumber) values(?)",(tempBuyOrderNumber,))
   # if i > 1000:
   #   break;
conn.commit()
conn.close()
print("End Time =", datetime.now().strftime("%H:%M:%S"))
        
                    
        
    
    