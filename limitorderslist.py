from datetime import datetime
import mysql.connector

mydb = mysql.connector.connect(
  host="localhost",
  port="3308",
  user="root",
  password="asd",
  database="iimu"
)
mycursor = mydb.cursor(buffered=True)
print("Start Time =", datetime.now().strftime("%H:%M:%S"))
startJiffiesSecond = datetime(1980, 1, 1, 0, 0, 0)
'''
Tranjuction Start: 2013-04-01 13:26:52.180331
Tranjuction End:   2013-04-01 20:23:46.305928
'''
transuctionDate = datetime(2013, 4, 1, 15, 15, 15)
dateDiff = transuctionDate - startJiffiesSecond
dateSec = dateDiff.total_seconds();
jiffiesSecondStart = dateSec*65535;
transuctionDate = datetime(2013, 4, 1, 15, 15, 19)
dateDiff = transuctionDate - startJiffiesSecond
dateSec = dateDiff.total_seconds();
jiffiesSecondEnd = dateSec*65535;
i=0
print(jiffiesSecondStart)
print(jiffiesSecondEnd)
sql = "SELECT MAX(transactionTime) as mx_transactionTime,orderNumber FROM orderraw   where transactionTime >= %s and  transactionTime <= %s group by orderNumber"
mycursor.execute(sql, (jiffiesSecondStart, jiffiesSecondEnd) )
allOrderM = mycursor.fetchall()
for eachOrderM in allOrderM:
    transactionTimeM = eachOrderM[0]
    orderNumberM = eachOrderM[1]
    sql = "insert into limitorderlist (orderNumber,transactionTime) values(%s,%s)"
    val = (orderNumberM,transactionTimeM)
    mycursor.execute(sql, val)
    i = i + 1
        
mydb.commit()
mycursor.close()
mydb.close() 
print("End Time =", datetime.now().strftime("%H:%M:%S"))
    