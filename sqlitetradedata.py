import gzip
from datetime import datetime
import sqlite3
conn = sqlite3.connect('iimsqlite.db')
rawFile = gzip.open("rawdata/CASH_Trades_01042013.DAT.gz", "rb")
print("Start Time =", datetime.now().strftime("%H:%M:%S"))
i=0
for eachLine in rawFile:
    i=i+1
    recordIndicator = eachLine[0:2].decode('utf8')
    segment = eachLine[2:6].decode('utf8')
    tradeNumber = eachLine[6:22].decode('utf8')
    tradeTime = eachLine[22:36].decode('utf8')
    symbol = eachLine[36:46].decode('utf8')
    series = eachLine[46:48].decode('utf8')
    tradePrice  = eachLine[48:56].decode('utf8')
    tradeQuantity  = eachLine[56:64].decode('utf8')
    buyOrderNumber  = eachLine[64:80].decode('utf8')
    buyAlgoIndicator = eachLine[80:81].decode('utf8')
    buyClientIdentityFlag = eachLine[81:82].decode('utf8')
    sellOrderNumber = eachLine[82:98].decode('utf8')
    sellAlgoIndicator = eachLine[98:99].decode('utf8')
    sellClientIdentityFlag = eachLine[99:100].decode('utf8')
    conn.execute("insert into traderaw (recordIndicator,segment,tradeNumber,tradeTime,symbol,series,tradePrice,tradeQuantity,buyOrderNumber,buyAlgoIndicator,buyAlgoIndicator,buyClientIdentityFlag,sellOrderNumber,sellAlgoIndicator,sellClientIdentityFlag) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",(recordIndicator,segment,tradeNumber,tradeTime,symbol,series,tradePrice,tradeQuantity,buyOrderNumber,buyAlgoIndicator,buyAlgoIndicator,buyClientIdentityFlag,sellOrderNumber,sellAlgoIndicator,sellClientIdentityFlag)) 

   # if i > 100:
   #   break;
conn.commit()
conn.close()    
rawFile.close()
print("End Time =", datetime.now().strftime("%H:%M:%S"))
    


