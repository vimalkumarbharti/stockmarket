import gzip
from datetime import datetime
import mysql.connector

mydb = mysql.connector.connect(
  host="localhost",
  port="3308",
  user="root",
  password="asd",
  database="iimu"
)
mycursor = mydb.cursor()
rawFile = gzip.open("companyorderdata/raw/SBIN_CASH_Orders.DAT.gz", "rb")
requiredOrder = [67314834,67348420,67359643,67345084,67345738,67312004,67317896,67279155,67307232,67277950,67513456,67511612,67261033,67384699,67707347,67478027,67261020,67472677,67124887,67123149,66758174,66758172,67194401,66878998,66768357,66768472,66892841,66927235,66927300,66927303,66946938,66966102,66948880,67012710,66995528]
sql =  "insert into orderrequireddata (recordIndicator,segment,orderNumber,transactionTime,buySellIndicator, activityType,symbol,series,volumeDisclosed,volumeOriginal,limitPrice,triggerPrice,marketOrderFlag,stopLossFlag,iOFlag,algoIndicator,clientIdentity) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
print("Start Time =", datetime.now().strftime("%H:%M:%S"))
i=0
for eachLine in rawFile:
    i=i+1
    tempOrderNumber  = int(eachLine[14:22].decode('utf8'))
    if tempOrderNumber in requiredOrder:
        recordIndicator = eachLine[0:2].decode('utf8')
        segment = eachLine[2:6].decode('utf8')
        orderNumber = eachLine[6:22].decode('utf8')
        transactionTime = eachLine[22:36].decode('utf8')
        buySellIndicator = eachLine[36:37].decode('utf8')
        activityType = eachLine[37:38].decode('utf8')
        symbol = eachLine[38:48].decode('utf8')
        series = eachLine[48:50].decode('utf8')
        volumeDisclosed = eachLine[50:58].decode('utf8')
        volumeOriginal = eachLine[58:66].decode('utf8')
        limitPrice = eachLine[66:74].decode('utf8')
        triggerPrice = eachLine[74:82].decode('utf8')
        marketOrderFlag = eachLine[82:83].decode('utf8')
        stopLossFlag = eachLine[83:84].decode('utf8')
        iOFlag  = eachLine[84:85].decode('utf8')
        algoIndicator =  eachLine[85:86].decode('utf8')
        clientIdentity =  eachLine[86:87].decode('utf8')
        val = (recordIndicator,segment,orderNumber,transactionTime,buySellIndicator, activityType,symbol,series,volumeDisclosed,volumeOriginal,limitPrice,triggerPrice,marketOrderFlag,stopLossFlag,iOFlag,algoIndicator,clientIdentity)
        mycursor.execute(sql, val)
    
    #if i > 10:
   #   break;
mydb.commit()
mycursor.close()
mydb.close()
rawFile.close()
print("End Time =", datetime.now().strftime("%H:%M:%S"))
    


