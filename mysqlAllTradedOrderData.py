from datetime import datetime
import mysql.connector
import pandas as pd
from sqlalchemy import create_engine
import pymysql
#from pandas.io import sql
#import MySQLdb

# Function to insert row in the dataframe 
def Insert_row(row_number, df, row_value): 
    # Starting value of upper half 
    start_upper = 0
   
    # End value of upper half 
    end_upper = row_number 
   
    # Start value of lower half 
    start_lower = row_number 
   
    # End value of lower half 
    end_lower = df.shape[0] 
   
    # Create a list of upper_half index 
    upper_half = [*range(start_upper, end_upper, 1)] 
   
    # Create a list of lower_half index 
    lower_half = [*range(start_lower, end_lower, 1)] 
   
    # Increment the value of lower half by 1 
    lower_half = [x.__add__(1) for x in lower_half] 
   
    # Combine the two lists 
    index_ = upper_half + lower_half 
   
    # Update the index of the dataframe 
    df.index = index_ 
   
    # Insert a row at the end 
    df.loc[row_number] = row_value 
    
    # Sort the index labels 
    df = df.sort_index() 
   
    # return the dataframe 
    return df 

mydb = mysql.connector.connect(
  host="localhost",
  port="3308",
  user="root",
  password="asd",
  database="iimu"
)
engine = create_engine("mysql+pymysql://{user}:{pw}@localhost:3308/{db}"
                       .format(user="root",
                               pw="asd",
                               db="iimu"))
mycursor = mydb.cursor(buffered=True) 
print("Start Time =", datetime.now().strftime("%H:%M:%S")) 
i=0 
mycursor.execute('SELECT DISTINCT ordernumber FROM ordertraded') 
allTradedOrder = mycursor.fetchall() 
mycursor.reset()
for eachTradedOrder in allTradedOrder :
    i=i+1
    tempOrderNumber = int(eachTradedOrder[0]) 
    SqlOrder = "SELECT * FROM allordertraded WHERE orderNumber = %s order by transactionTime ASC"
    adr = (tempOrderNumber, )
    mycursor.execute(SqlOrder,adr)
    allOrder = mycursor.fetchall() 
    # Put it all to a data frame
    sql_data = pd.DataFrame(allOrder)
    sql_data.columns = mycursor.column_names
    # Show the data
    # print(sql_data)
    row_sql_data = len(sql_data)
    #print(row_sql_data)
    j = 1
    while(j < row_sql_data):
        if (int(sql_data.iloc[j, 6])==5) and (int(sql_data.iloc[j-1, 10]) > int(sql_data.iloc[j, 10])):
            Insert_row(j,sql_data,sql_data.iloc[j-1,:])
            sql_data.iloc[row_sql_data, 0]=int(sql_data.iloc[j, 0])+ 1000000000
            sql_data.iloc[row_sql_data, 6]=6
            sql_data.iloc[row_sql_data, 4]=sql_data.iloc[j, 4] + 1
            sql_data.iloc[row_sql_data, 10] = int(sql_data.iloc[j-1, 10]) - int(sql_data.iloc[j, 10])        
            j = j+2
            row_sql_data = len(sql_data)
            sql_data = sql_data.sort_values(by=['transactionTime'])
        else:
            j = j + 1 
    sql_dataU = pd.DataFrame(sql_data.loc[sql_data['activityType'] == 6])
    row_sql_dataU = len(sql_dataU)
    if (row_sql_dataU > 0):
        sql_dataU.to_sql('allordertraded', con = engine, if_exists = 'append', chunksize = 1000,index= False)
     #   for j1 in range(row_sql_dataU) : 
     #       print(sql_dataU.iloc[j1, 0], sql_dataU.iloc[j1, 4], sql_dataU.iloc[j1, 6], sql_dataU.iloc[j1, 10]) 
     #  if i > 20: 
     #      break;
        
mydb.commit()
mycursor.close()
mydb.close() 
print("End Time =", datetime.now().strftime("%H:%M:%S"))