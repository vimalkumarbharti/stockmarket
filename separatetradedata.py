# -*- coding: utf-8 -*-
import gzip
from datetime import datetime
rawFile = gzip.open("rawdata/CASH_Trades_01042013.DAT.gz", "rb")
companyfiles = {}
i=0
print("Start Time =", datetime.now().strftime("%H:%M:%S"))
for eachLine in rawFile:
    i=i+1
    symbol = eachLine[36:46]
    companyName = symbol.decode('utf8').lstrip("b")
    if companyName in companyfiles.keys():
        companyFileName = companyfiles[companyName][0]
        companyFileObj = companyfiles[companyName][1]
    else:
        companyFileName = "companytradedata/raw/"+companyName+"_CASH_Trades.DAT.gz"
        companyFileObj = gzip.open(companyFileName, "wb")
        companyfiles[companyName] = [companyFileName,companyFileObj]
  
    companyFileObj.write(eachLine)
   # print(companyName)
   # if i > 100:
   #   break;
for companydata in companyfiles:  
    companyfiles[companydata][1].close()
rawFile.close()
print("End Time =", datetime.now().strftime("%H:%M:%S"))
    
